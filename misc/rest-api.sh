curl -X GET  'http://localhost:8080/o/c/courseregistrations/scopes/33338' \
  -H 'accept: application/json' \
  -u test@liferay.com:test

curl -X GET  'http://localhost:8080/o/c/courseregistrations/scopes/33338?fields=id%2Cnote' \
  -H 'accept: application/json' \
  -u test@liferay.com:test

curl -X GET  'http://localhost:8080/o/c/courseregistrations/scopes/33338?fields=id%2Cnote%2CregistrationStatus&filter=registrationStatus%20eq%20%27approved%27' \
  -H 'accept: application/json' \
  -u test@liferay.com:test

curl -X GET  'http://localhost:8080/o/c/courseregistrations/scopes/33338?fields=id%2Cnote%2CregistrationStatus&filter=registrationStatus%20eq%20%27denied%27' \
  -H 'accept: application/json' \
  -u test@liferay.com:test

