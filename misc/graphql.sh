curl 'http://localhost:8080/o/graphql' -X POST \
-u test@liferay.com:test \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
-d '{ "query":"
    {
      c {
        courseRegistrations(scopeKey: \"33338\") {
          items {courseRegistrationId, note}
        }
      }
    }
"}'

curl -L 'http://localhost:8080/o/graphql' \
-H 'Content-Type: application/json' \
-u test@liferay.com:test \
-d '{
  "query":
    "mutation {
      c {
        deleteCourseRegistration(courseRegistrationId: 36288)
      }
    }"
}'
