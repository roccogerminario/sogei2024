/**
 * Copyright (c) SMC Treviso Srl. All rights reserved.
 */

package it.smc.lrtraining.gogo.shell.message.users;

import com.liferay.portal.kernel.service.UserLocalService;


import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferencePolicyOption;

import it.smc.lrtraining.gogo.shell.message.api.GogoShellMessage;

/**
 * @author Rocco Germinario
 */
@Component(
	immediate = true,
	property = {"service.ranking:Integer=10"},
	service = GogoShellMessage.class
)
public class GogoShellMessageUsers implements GogoShellMessage {

	@Override
	public String getMessage() {

		String message =
			"There are " + _userLocalService.getUsersCount() + " users";
		return message;
	}

	@Reference(policyOption = ReferencePolicyOption.GREEDY)
	private UserLocalService _userLocalService;

}
