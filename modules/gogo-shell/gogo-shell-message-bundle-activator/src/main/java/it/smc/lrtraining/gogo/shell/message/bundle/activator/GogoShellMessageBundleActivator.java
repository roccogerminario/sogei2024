package it.smc.lrtraining.gogo.shell.message.bundle.activator;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import it.smc.lrtraining.gogo.shell.message.api.GogoShellMessage;

/**
 * @author Rocco Germinario
 * @author Michele Perissinotto
 */
public class GogoShellMessageBundleActivator implements BundleActivator {

	@Override
	public void start(BundleContext bundleContext) throws Exception {
		System.out.println("Starting bundle " + bundleContext.getBundle());

		ServiceTracker<GogoShellMessage, GogoShellMessage> serviceTracker =
			ServiceTrackerFactory.open(
				bundleContext.getBundle(), GogoShellMessage.class);

		GogoShellMessage gogoShellMessage = serviceTracker.getService();

		serviceTracker.close();

		if (gogoShellMessage != null) {
			System.out.println(gogoShellMessage.getMessage());
		}

	}

	@Override
	public void stop(BundleContext bundleContext) throws Exception {
		System.out.println("Stopping bundle " + bundleContext.getBundle());
	}

}
