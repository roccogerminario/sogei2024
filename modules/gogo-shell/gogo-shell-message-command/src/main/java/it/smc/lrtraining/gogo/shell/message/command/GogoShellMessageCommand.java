package it.smc.lrtraining.gogo.shell.message.command;

import java.lang.Object;

import it.smc.lrtraining.gogo.shell.message.api.GogoShellMessage;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicyOption;

/**
 * @author rge
 */
@Component(
	immediate = true,
	property = {
		"osgi.command.function=print",
		"osgi.command.scope=corso"},
	service = Object.class
)
public class GogoShellMessageCommand {

	public void print() {

		if (_gogoShellMessage != null) {
			String message = _gogoShellMessage.getMessage();
			System.out.println(message);
		}
		else {
			System.out.println("Nessuna implementazione disponibile.");
		}
	}

	@Reference(
		cardinality = ReferenceCardinality.OPTIONAL,
		policyOption = ReferencePolicyOption.GREEDY)
	public void setGogoShellMessage(
		GogoShellMessage gogoShellMessage) {

		_gogoShellMessage = gogoShellMessage;
	}

	private GogoShellMessage _gogoShellMessage;

}
