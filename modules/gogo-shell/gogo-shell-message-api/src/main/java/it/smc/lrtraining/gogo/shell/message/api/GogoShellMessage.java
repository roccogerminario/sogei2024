package it.smc.lrtraining.gogo.shell.message.api;

/**
 * @author rge
 */
public interface GogoShellMessage {
	public String getMessage();
}
