package it.smc.lrtraining.gogo.shell.message.impl;

import it.smc.lrtraining.gogo.shell.message.api.GogoShellMessage;

import org.osgi.service.component.annotations.Component;

/**
 * @author rge
 */
@Component(
	immediate = true,
	service = GogoShellMessage.class
)
public class GogoShellMessageImpl implements GogoShellMessage {

	@Override
	public String getMessage() {

		return "Hello from GogoShellMessageImpl";
	}

}
