<%--
/**
 * Copyright (c) SMC Treviso Srl. All rights reserved.
 */
--%>

<%@ include file="init.jsp" %>

<%
boolean showAddTaskButton = ManageTasksPermission.contains(permissionChecker, scopeGroupId, ActionKeys.ADD_TASK);
boolean showPermissionsButton = ManageTasksPermission.contains(permissionChecker, scopeGroupId, ActionKeys.PERMISSIONS);
%>

<c:if test="<%= showAddTaskButton || showPermissionsButton %>">
	<aui:button-row>
		<c:if test="<%= showAddTaskButton %>">
			<liferay-portlet:renderURL var="addTaskURL">
				<liferay-portlet:param name="mvcPath" value="/edit_task.jsp" />
				<liferay-portlet:param name="redirect" value="<%= currentURL %>" />
			</liferay-portlet:renderURL>

			<aui:button href="<%= addTaskURL %>" icon="icon-plus" value="add-task" />
		</c:if>

		<c:if test="<%= showPermissionsButton %>">
			<liferay-security:permissionsURL
				modelResource="it.smc.lrtraining.manage.tasks"
				modelResourceDescription="<%= HtmlUtil.escape(themeDisplay.getScopeGroupName()) %>"
				resourcePrimKey="<%= String.valueOf(scopeGroupId) %>"
				var="permissionsURL"
				windowState="<%= LiferayWindowState.POP_UP.toString() %>"
			/>

			<aui:button href="<%= permissionsURL %>" useDialog="<%= true %>" value="permissions" />
		</c:if>
	</aui:button-row>
</c:if>

<liferay-ui:success key="task-added" message="task-added" />
<liferay-ui:success key="task-updated" message="task-updated" />

<div class="container-fluid-1280">

		<liferay-ui:search-container
			delta="10"
			emptyResultsMessage="no-tasks-were-found"
			total="<%= TaskServiceUtil.getTasksCount(company.getCompanyId(), scopeGroupId) %>"
			>

			<liferay-ui:search-container-results
				results="<%= TaskServiceUtil.getTasks(company.getCompanyId(), scopeGroupId, searchContainer.getStart(), searchContainer.getEnd()) %>"
			/>

			<liferay-ui:search-container-row
				className="it.smc.lrtraining.manage.tasks.model.Task"
				keyProperty="taskId"
				modelVar="task">

				<liferay-ui:search-container-column-text
					cssClass="table-cell-content"
					name="title"
					property="title"
				/>
				<liferay-ui:search-container-column-text
					cssClass="table-cell-content"
					name="description"
					property="description"
				/>
				<liferay-ui:search-container-column-text
					name="expiration-date"
					value="<%= dateFormat.format(task.getExpirationDate()) %>"
				/>
				 <liferay-ui:search-container-column-text
					name="task-user"
					value="<%= HtmlUtil.escape(task.getTaskUserFullName()) %>"
				/>
				<liferay-ui:search-container-column-text
					name="completed"
					property="completed"
				/>
				<liferay-ui:search-container-column-jsp
					path="/task_action.jsp"
				/>
			</liferay-ui:search-container-row>
			<liferay-ui:search-iterator markupView="lexicon" />
		</liferay-ui:search-container>
</div>
