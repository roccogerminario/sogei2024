/**
 * Copyright (c) SMC Treviso Srl. All rights reserved.
 */

package it.smc.lrtraining.manage.tasks.web.constants;

import com.liferay.portal.kernel.util.WebKeys;

/**
 * @author SMC Treviso
 */
public interface ManageTasksWebKeys extends WebKeys {

	public static final String TASK = "TASK";

}
