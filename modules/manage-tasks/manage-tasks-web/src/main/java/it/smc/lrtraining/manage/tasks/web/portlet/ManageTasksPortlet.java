package it.smc.lrtraining.manage.tasks.web.portlet;

import it.smc.lrtraining.manage.tasks.model.Task;
import it.smc.lrtraining.manage.tasks.service.TaskService;
import it.smc.lrtraining.manage.tasks.constants.ManageTasksPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferencePolicyOption;

/**
 * @author rge
 */
@Component(
	property = {
		"com.liferay.portlet.add-default-resource=true",
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=ManageTasks",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ManageTasksPortletKeys.MANAGE_TASKS,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class ManageTasksPortlet extends MVCPortlet {

	public void deleteTask(
			ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		long taskId = ParamUtil
			.getLong(actionRequest, "taskId");

		_taskService.deleteTask(taskId);

		hideDefaultSuccessMessage(actionRequest);
	}

	public void updateTask(
		ActionRequest actionRequest, ActionResponse actionResponse)
		throws Exception {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
			Task.class.getName(), actionRequest);

		long taskId = ParamUtil.getLong(actionRequest, "taskId");

		String title = ParamUtil.getString(actionRequest, "title");
		String description = ParamUtil.getString(actionRequest, "description");
		long taskUserId = ParamUtil.getLong(actionRequest, "taskUserId");
		boolean completed = ParamUtil.getBoolean(actionRequest, "completed");

		int expirationDateMonth = ParamUtil.getInteger(
			actionRequest, "expirationDateMonth");
		int expirationDateDay = ParamUtil.getInteger(
			actionRequest, "expirationDateDay");
		int expirationDateYear = ParamUtil.getInteger(
			actionRequest, "expirationDateYear");

		if (taskId > 0) {
			_taskService.updateTask(
				taskId, title, description, expirationDateMonth,
				expirationDateDay, expirationDateYear, taskUserId, completed,
				serviceContext);
		}
		else {
			_taskService.addTask(title, description, expirationDateMonth,
				expirationDateDay, expirationDateYear, taskUserId,
				completed, serviceContext);
		}

		//		hideDefaultSuccessMessage(actionRequest);

	}

	@Reference(policyOption = ReferencePolicyOption.GREEDY)
	private TaskService _taskService;


}
