package it.smc.lrtraining.manage.tasks.constants;

/**
 * @author rge
 */
public class ManageTasksPortletKeys {

	public static final String MANAGE_TASKS =
		"it_smc_lrtraining_manage_tasks_web_ManageTasksPortlet";

}
