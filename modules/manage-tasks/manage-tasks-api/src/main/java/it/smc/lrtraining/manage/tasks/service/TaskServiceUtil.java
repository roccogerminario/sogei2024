/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package it.smc.lrtraining.manage.tasks.service;

import com.liferay.portal.kernel.exception.PortalException;

import it.smc.lrtraining.manage.tasks.model.Task;

import java.util.List;

/**
 * Provides the remote service utility for Task. This utility wraps
 * <code>it.smc.lrtraining.manage.tasks.service.impl.TaskServiceImpl</code> and is an
 * access point for service operations in application layer code running on a
 * remote server. Methods of this service are expected to have security checks
 * based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see TaskService
 * @generated
 */
public class TaskServiceUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to <code>it.smc.lrtraining.manage.tasks.service.impl.TaskServiceImpl</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static Task addTask(
			String title, String description, int expirationDateMonth,
			int expirationDateDay, int expirationDateYear, long taskUserId,
			boolean completed,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().addTask(
			title, description, expirationDateMonth, expirationDateDay,
			expirationDateYear, taskUserId, completed, serviceContext);
	}

	public static Task deleteTask(long taskId) throws PortalException {
		return getService().deleteTask(taskId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static Task getTask(long taskId) throws PortalException {
		return getService().getTask(taskId);
	}

	public static List<Task> getTasks(
		long companyId, long groupId, int start, int end) {

		return getService().getTasks(companyId, groupId, start, end);
	}

	public static int getTasksCount(long companyId, long groupId) {
		return getService().getTasksCount(companyId, groupId);
	}

	public static Task updateTask(
			long taskId, String title, String description,
			int expirationDateMonth, int expirationDateDay,
			int expirationDateYear, long taskUserId, boolean completed,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws PortalException {

		return getService().updateTask(
			taskId, title, description, expirationDateMonth, expirationDateDay,
			expirationDateYear, taskUserId, completed, serviceContext);
	}

	public static TaskService getService() {
		return _service;
	}

	public static void setService(TaskService service) {
		_service = service;
	}

	private static volatile TaskService _service;

}