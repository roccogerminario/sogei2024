/**
 * Copyright (c) SMC Treviso Srl. All rights reserved.
 */

package it.smc.lrtraining.manage.tasks.constants;

public class ActionKeys
	extends com.liferay.portal.kernel.security.permission.ActionKeys {

	public static final String ADD_TASK = "ADD_TASK";

}
