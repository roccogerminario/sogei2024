/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package it.smc.lrtraining.manage.tasks.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link TaskService}.
 *
 * @author Brian Wing Shun Chan
 * @see TaskService
 * @generated
 */
public class TaskServiceWrapper
	implements ServiceWrapper<TaskService>, TaskService {

	public TaskServiceWrapper() {
		this(null);
	}

	public TaskServiceWrapper(TaskService taskService) {
		_taskService = taskService;
	}

	@Override
	public it.smc.lrtraining.manage.tasks.model.Task addTask(
			String title, String description, int expirationDateMonth,
			int expirationDateDay, int expirationDateYear, long taskUserId,
			boolean completed,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _taskService.addTask(
			title, description, expirationDateMonth, expirationDateDay,
			expirationDateYear, taskUserId, completed, serviceContext);
	}

	@Override
	public it.smc.lrtraining.manage.tasks.model.Task deleteTask(long taskId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _taskService.deleteTask(taskId);
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _taskService.getOSGiServiceIdentifier();
	}

	@Override
	public it.smc.lrtraining.manage.tasks.model.Task getTask(long taskId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _taskService.getTask(taskId);
	}

	@Override
	public java.util.List<it.smc.lrtraining.manage.tasks.model.Task> getTasks(
		long companyId, long groupId, int start, int end) {

		return _taskService.getTasks(companyId, groupId, start, end);
	}

	@Override
	public int getTasksCount(long companyId, long groupId) {
		return _taskService.getTasksCount(companyId, groupId);
	}

	@Override
	public it.smc.lrtraining.manage.tasks.model.Task updateTask(
			long taskId, String title, String description,
			int expirationDateMonth, int expirationDateDay,
			int expirationDateYear, long taskUserId, boolean completed,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _taskService.updateTask(
			taskId, title, description, expirationDateMonth, expirationDateDay,
			expirationDateYear, taskUserId, completed, serviceContext);
	}

	@Override
	public TaskService getWrappedService() {
		return _taskService;
	}

	@Override
	public void setWrappedService(TaskService taskService) {
		_taskService = taskService;
	}

	private TaskService _taskService;

}