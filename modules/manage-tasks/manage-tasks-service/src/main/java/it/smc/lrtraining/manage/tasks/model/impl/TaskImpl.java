/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package it.smc.lrtraining.manage.tasks.model.impl;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.exception.NoSuchUserException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalServiceUtil;

/**
 * @author Brian Wing Shun Chan
 */
public class TaskImpl extends TaskBaseImpl {
	public String getTaskUserFullName()
		throws PortalException, SystemException {

		try {
			User user = UserLocalServiceUtil.getUser(getTaskUserId());

			return user.getFullName();
		}
		catch (NoSuchUserException nsue) {
			return StringPool.BLANK;
		}
	}
}
