/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package it.smc.lrtraining.manage.tasks.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.security.permission.resource.PortletResourcePermission;
import com.liferay.portal.kernel.service.ServiceContext;

import java.util.List;

import it.smc.lrtraining.manage.tasks.constants.ActionKeys;
import it.smc.lrtraining.manage.tasks.constants.ManageTasksConstants;
import it.smc.lrtraining.manage.tasks.model.Task;
import it.smc.lrtraining.manage.tasks.service.base.TaskServiceBaseImpl;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.service.component.annotations.ReferencePolicyOption;

/**
 * @author Brian Wing Shun Chan
 */
@Component(
	property = {
		"json.web.service.context.name=mt", "json.web.service.context.path=Task"
	},
	service = AopService.class
)
public class TaskServiceImpl extends TaskServiceBaseImpl {
	public Task addTask(
		String title, String description, int expirationDateMonth,
		int expirationDateDay, int expirationDateYear,
		long taskUserId, boolean completed,
		ServiceContext serviceContext)
		throws PortalException {

		_portletResourcePermission.check(getPermissionChecker(),
			serviceContext.getScopeGroupId(), ActionKeys.ADD_TASK);

		return taskLocalService.addTask(title, description, expirationDateMonth,
			expirationDateDay, expirationDateYear, taskUserId,
			completed, serviceContext);
	}

	public Task deleteTask(long taskId) throws PortalException {
		_taskModelResourcePermission.check(
			getPermissionChecker(), taskId, ActionKeys.DELETE);

		return taskLocalService.deleteTask(taskId);
	}

	public Task getTask(long taskId) throws PortalException {
		_taskModelResourcePermission.check(
			getPermissionChecker(), taskId, ActionKeys.VIEW);

		return taskLocalService.getTask(taskId);
	}

	public List<Task> getTasks(
		long companyId, long groupId, int start, int end) {

		return taskPersistence.filterFindByC_G(companyId, groupId, start, end);
	}

	public int getTasksCount(long companyId, long groupId) {
		return taskPersistence.filterCountByC_G(companyId, groupId);
	}

	public Task updateTask(
		long taskId, String title, String description,
		int expirationDateMonth, int expirationDateDay,
		int expirationDateYear, long taskUserId, boolean completed,
		ServiceContext serviceContext)
		throws PortalException {

		_taskModelResourcePermission.check(
			getPermissionChecker(), taskId, ActionKeys.UPDATE);

		return taskLocalService.updateTask(
			taskId, title, description, expirationDateMonth,
			expirationDateDay, expirationDateYear, taskUserId, completed,
			serviceContext);
	}

	@Reference(
		policy = ReferencePolicy.DYNAMIC,
		policyOption = ReferencePolicyOption.GREEDY,
		target = "(model.class.name=" + ManageTasksConstants.TASK + ")"
	)
	private volatile ModelResourcePermission<Task> _taskModelResourcePermission;

	@Reference(
		policy = ReferencePolicy.DYNAMIC,
		policyOption = ReferencePolicyOption.GREEDY,
		target = "(resource.name=" + ManageTasksConstants.RESOURCE_NAME + ")"
	)
	private volatile PortletResourcePermission _portletResourcePermission;

}
