/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package it.smc.lrtraining.manage.tasks.service.impl;

import com.liferay.portal.aop.AopService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.ResourceConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.Validator;

import java.util.Date;
import java.util.List;

import it.smc.lrtraining.manage.tasks.exception.TaskTitleException;
import it.smc.lrtraining.manage.tasks.model.Task;
import it.smc.lrtraining.manage.tasks.service.base.TaskLocalServiceBaseImpl;

import org.osgi.service.component.annotations.Component;

/**
 * @author Brian Wing Shun Chan
 */
@Component(
	property = "model.class.name=it.smc.lrtraining.manage.tasks.model.Task",
	service = AopService.class
)
public class TaskLocalServiceImpl extends TaskLocalServiceBaseImpl {
	@Override
	public Task addTask(
			String title, String description,
			int expirationDateMonth, int expirationDateDay,
			int expirationDateYear, long taskUserId, boolean completed,
			ServiceContext serviceContext)
		throws PortalException {

		User user = userLocalService.getUser(serviceContext.getUserId());
		userLocalService.getUser(taskUserId);

		long groupId = serviceContext.getScopeGroupId();

		validate(title);

		Date now = new Date();

		long taskId = counterLocalService.increment();

		Task task = createTask(taskId);

		task.setUuid(serviceContext.getUuid());

		// Audit fields

		task.setGroupId(groupId);
		task.setCompanyId(serviceContext.getCompanyId());
		task.setUserId(user.getUserId());
		task.setUserName(user.getFullName());
		task.setCreateDate(serviceContext.getCreateDate(now));
		task.setModifiedDate(serviceContext.getModifiedDate(now));

		// Other fields

		Date expirationDate = PortalUtil.getDate(
			expirationDateMonth, expirationDateDay, expirationDateYear);

		task.setTitle(title);
		task.setDescription(description);
		task.setExpirationDate(expirationDate);
		task.setTaskUserId(taskUserId);
		task.setCompleted(completed);

		task = taskPersistence.update(task);

		resourceLocalService.addResources(
			task.getCompanyId(), task.getGroupId(), task.getUserId(),
			Task.class.getName(), task.getTaskId(), false, true, true);

		return task;
	}

	@Override
	public Task deleteTask(long taskId)
		throws PortalException, SystemException {

		Task task = taskPersistence.findByPrimaryKey(taskId);

		return deleteTask(task);
	}

	@Override
	public Task deleteTask(Task task)
		throws SystemException, PortalException {

		task = taskPersistence.remove(task);

		// Resources

		resourceLocalService.deleteResource(
			task.getCompanyId(), Task.class.getName(),
			ResourceConstants.SCOPE_INDIVIDUAL, task.getTaskId());

		return task;
	}

	public List<Task> getTasks(long companyId, long groupId, int start, int end)
		throws SystemException {

		return taskPersistence.findByC_G(companyId, groupId, start, end);
	}

	public int getTasksCount(long companyId, long groupId)
		throws SystemException {

		return taskPersistence.countByC_G(companyId, groupId);
	}


	@Override
	public Task updateTask(
			long taskId, String title, String description,
			int expirationDateMonth, int expirationDateDay,
			int expirationDateYear, long taskUserId, boolean completed,
			ServiceContext serviceContext)
		throws PortalException {

		// Task

		userLocalService.getUser(taskUserId);
		Task task = taskPersistence.findByPrimaryKey(taskId);

		validate(title);

		Date now = new Date();

		// Audit fields

		task.setModifiedDate(serviceContext.getModifiedDate(now));

		// Other fields

		Date expirationDate = PortalUtil.getDate(
			expirationDateMonth, expirationDateDay, expirationDateYear);

		task.setTitle(title);
		task.setDescription(description);
		task.setExpirationDate(expirationDate);
		task.setTaskUserId(taskUserId);
		task.setCompleted(completed);

		task = taskPersistence.update(task);

		return task;
	}

	protected void validate(String title) throws PortalException {
		if (Validator.isNull(title)) {
			throw new TaskTitleException();
		}
	}

}
