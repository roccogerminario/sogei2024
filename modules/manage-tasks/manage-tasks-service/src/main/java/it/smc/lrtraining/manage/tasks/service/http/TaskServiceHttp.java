/**
 * SPDX-FileCopyrightText: (c) 2024 Liferay, Inc. https://liferay.com
 * SPDX-License-Identifier: LGPL-2.1-or-later OR LicenseRef-Liferay-DXP-EULA-2.0.0-2023-06
 */

package it.smc.lrtraining.manage.tasks.service.http;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.HttpPrincipal;
import com.liferay.portal.kernel.service.http.TunnelUtil;
import com.liferay.portal.kernel.util.MethodHandler;
import com.liferay.portal.kernel.util.MethodKey;

import it.smc.lrtraining.manage.tasks.service.TaskServiceUtil;

/**
 * Provides the HTTP utility for the
 * <code>TaskServiceUtil</code> service
 * utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it requires an additional
 * <code>HttpPrincipal</code> parameter.
 *
 * <p>
 * The benefits of using the HTTP utility is that it is fast and allows for
 * tunneling without the cost of serializing to text. The drawback is that it
 * only works with Java.
 * </p>
 *
 * <p>
 * Set the property <b>tunnel.servlet.hosts.allowed</b> in portal.properties to
 * configure security.
 * </p>
 *
 * <p>
 * The HTTP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class TaskServiceHttp {

	public static it.smc.lrtraining.manage.tasks.model.Task addTask(
			HttpPrincipal httpPrincipal, String title, String description,
			int expirationDateMonth, int expirationDateDay,
			int expirationDateYear, long taskUserId, boolean completed,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		try {
			MethodKey methodKey = new MethodKey(
				TaskServiceUtil.class, "addTask", _addTaskParameterTypes0);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, title, description, expirationDateMonth,
				expirationDateDay, expirationDateYear, taskUserId, completed,
				serviceContext);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				if (exception instanceof
						com.liferay.portal.kernel.exception.PortalException) {

					throw (com.liferay.portal.kernel.exception.PortalException)
						exception;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (it.smc.lrtraining.manage.tasks.model.Task)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static it.smc.lrtraining.manage.tasks.model.Task deleteTask(
			HttpPrincipal httpPrincipal, long taskId)
		throws com.liferay.portal.kernel.exception.PortalException {

		try {
			MethodKey methodKey = new MethodKey(
				TaskServiceUtil.class, "deleteTask",
				_deleteTaskParameterTypes1);

			MethodHandler methodHandler = new MethodHandler(methodKey, taskId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				if (exception instanceof
						com.liferay.portal.kernel.exception.PortalException) {

					throw (com.liferay.portal.kernel.exception.PortalException)
						exception;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (it.smc.lrtraining.manage.tasks.model.Task)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static it.smc.lrtraining.manage.tasks.model.Task getTask(
			HttpPrincipal httpPrincipal, long taskId)
		throws com.liferay.portal.kernel.exception.PortalException {

		try {
			MethodKey methodKey = new MethodKey(
				TaskServiceUtil.class, "getTask", _getTaskParameterTypes2);

			MethodHandler methodHandler = new MethodHandler(methodKey, taskId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				if (exception instanceof
						com.liferay.portal.kernel.exception.PortalException) {

					throw (com.liferay.portal.kernel.exception.PortalException)
						exception;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (it.smc.lrtraining.manage.tasks.model.Task)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static java.util.List<it.smc.lrtraining.manage.tasks.model.Task>
		getTasks(
			HttpPrincipal httpPrincipal, long companyId, long groupId,
			int start, int end) {

		try {
			MethodKey methodKey = new MethodKey(
				TaskServiceUtil.class, "getTasks", _getTasksParameterTypes3);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, companyId, groupId, start, end);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (java.util.List<it.smc.lrtraining.manage.tasks.model.Task>)
				returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static int getTasksCount(
		HttpPrincipal httpPrincipal, long companyId, long groupId) {

		try {
			MethodKey methodKey = new MethodKey(
				TaskServiceUtil.class, "getTasksCount",
				_getTasksCountParameterTypes4);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, companyId, groupId);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return ((Integer)returnObj).intValue();
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	public static it.smc.lrtraining.manage.tasks.model.Task updateTask(
			HttpPrincipal httpPrincipal, long taskId, String title,
			String description, int expirationDateMonth, int expirationDateDay,
			int expirationDateYear, long taskUserId, boolean completed,
			com.liferay.portal.kernel.service.ServiceContext serviceContext)
		throws com.liferay.portal.kernel.exception.PortalException {

		try {
			MethodKey methodKey = new MethodKey(
				TaskServiceUtil.class, "updateTask",
				_updateTaskParameterTypes5);

			MethodHandler methodHandler = new MethodHandler(
				methodKey, taskId, title, description, expirationDateMonth,
				expirationDateDay, expirationDateYear, taskUserId, completed,
				serviceContext);

			Object returnObj = null;

			try {
				returnObj = TunnelUtil.invoke(httpPrincipal, methodHandler);
			}
			catch (Exception exception) {
				if (exception instanceof
						com.liferay.portal.kernel.exception.PortalException) {

					throw (com.liferay.portal.kernel.exception.PortalException)
						exception;
				}

				throw new com.liferay.portal.kernel.exception.SystemException(
					exception);
			}

			return (it.smc.lrtraining.manage.tasks.model.Task)returnObj;
		}
		catch (com.liferay.portal.kernel.exception.SystemException
					systemException) {

			_log.error(systemException, systemException);

			throw systemException;
		}
	}

	private static Log _log = LogFactoryUtil.getLog(TaskServiceHttp.class);

	private static final Class<?>[] _addTaskParameterTypes0 = new Class[] {
		String.class, String.class, int.class, int.class, int.class, long.class,
		boolean.class, com.liferay.portal.kernel.service.ServiceContext.class
	};
	private static final Class<?>[] _deleteTaskParameterTypes1 = new Class[] {
		long.class
	};
	private static final Class<?>[] _getTaskParameterTypes2 = new Class[] {
		long.class
	};
	private static final Class<?>[] _getTasksParameterTypes3 = new Class[] {
		long.class, long.class, int.class, int.class
	};
	private static final Class<?>[] _getTasksCountParameterTypes4 =
		new Class[] {long.class, long.class};
	private static final Class<?>[] _updateTaskParameterTypes5 = new Class[] {
		long.class, String.class, String.class, int.class, int.class, int.class,
		long.class, boolean.class,
		com.liferay.portal.kernel.service.ServiceContext.class
	};

}