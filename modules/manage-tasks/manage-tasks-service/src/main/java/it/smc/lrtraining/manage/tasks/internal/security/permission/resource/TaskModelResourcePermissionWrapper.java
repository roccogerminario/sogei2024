/**
 * Unpublished Copyright SMC TREVISO SRL. All rights reserved.
 * <p>
 * The contents of this file are subject to the terms of the SMC TREVISO's
 * "CONDIZIONI GENERALI DI LICENZA D'USO DI SOFTWARE APPLICATIVO STANDARD SMC"
 * ("License"). You may not use this file except in compliance with the License.
 * You can obtain a copy of the License by contacting SMC TREVISO. See the
 * License for the limitations under the License, including but not limited to
 * distribution rights of the Software. You may not - for example - copy,
 * modify, transfer, transmit or distribute the whole file or portion of it, or
 * derived works, to a third party, except as may be permitted by SMC in a
 * written agreement.
 * To the maximum extent permitted by applicable law, this file is provided
 * "as is" without warranty of any kind, either expressed or implied, including
 * but not limited to, the implied warranty of merchantability, non infringement
 * and fitness for a particular purpose. SMC does not guarantee that the use of
 * the file will not be interrupted or error free.
 */

package it.smc.lrtraining.manage.tasks.internal.security.permission.resource;

import com.liferay.blogs.constants.BlogsPortletKeys;
import com.liferay.exportimport.kernel.staging.permission.StagingPermission;
import com.liferay.portal.kernel.security.permission.resource.BaseModelResourcePermissionWrapper;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermission;
import com.liferay.portal.kernel.security.permission.resource.ModelResourcePermissionFactory;
import com.liferay.portal.kernel.security.permission.resource.PortletResourcePermission;
import com.liferay.portal.kernel.security.permission.resource.StagedModelPermissionLogic;
import com.liferay.portal.kernel.security.permission.resource.WorkflowedModelPermissionLogic;
import com.liferay.portal.kernel.service.GroupLocalService;
import com.liferay.sharing.security.permission.resource.SharingModelResourcePermissionConfigurator;

import it.smc.lrtraining.manage.tasks.constants.ManageTasksConstants;
import it.smc.lrtraining.manage.tasks.constants.ManageTasksPortletKeys;
import it.smc.lrtraining.manage.tasks.model.Task;
import it.smc.lrtraining.manage.tasks.service.TaskLocalService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author Rocco Germinario
 */
@Component(
	property = "model.class.name=it.smc.lrtraining.manage.tasks.model.Task",
	service = ModelResourcePermission.class
)
public class TaskModelResourcePermissionWrapper
	extends BaseModelResourcePermissionWrapper<Task> {

	@Override
	protected ModelResourcePermission<Task> doGetModelResourcePermission() {

		return ModelResourcePermissionFactory.create(
			Task.class, Task::getTaskId, _taskLocalService::getTask,
			_portletResourcePermission, (modelResourcePermission, consumer) -> {
				consumer.accept(
					new StagedModelPermissionLogic<>(
						_stagingPermission, ManageTasksPortletKeys.MANAGE_TASKS,
						Task::getTaskId));
				consumer.accept(
					new WorkflowedModelPermissionLogic<>(
						modelResourcePermission, _groupLocalService,
						Task::getTaskId));

				_sharingModelResourcePermissionConfigurator.configure(
					modelResourcePermission, consumer);
			}
		);
	}

	@Reference
	private GroupLocalService _groupLocalService;

	@Reference(target = "(resource.name=" + ManageTasksConstants.RESOURCE_NAME + ")")
	private PortletResourcePermission _portletResourcePermission;

	@Reference
	private TaskLocalService _taskLocalService;

	@Reference
	private SharingModelResourcePermissionConfigurator
		_sharingModelResourcePermissionConfigurator;

	@Reference
	private StagingPermission _stagingPermission;
}
