create index IX_C8760FC5 on MT_Task (companyId, groupId);
create index IX_7754A8F7 on MT_Task (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_B1110139 on MT_Task (uuid_[$COLUMN_LENGTH:75$], groupId);