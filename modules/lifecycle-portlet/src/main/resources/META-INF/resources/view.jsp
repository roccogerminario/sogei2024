<%@ include file="/init.jsp" %>

<p>Lifecycle Test Portlet in View mode.</p>

<!-- Render URL-->
<p><a href="<portlet:renderURL/>">Test render</a></p>

<!-- Action URL -->
<p><a href="<portlet:actionURL/>">Test action</a></p>

<!-- Resource URL -->
<div style="font: 16pt courier" id="<portlet:namespace />myDiv"></div>

<button id="<portlet:namespace />myButton">Test resource (AJAX)</button>

<aui:script>
	var myButton = document.getElementById(
		'<portlet:namespace />myButton'
	);

	if (myButton) {
		myButton.addEventListener('click', (event) => {
			Liferay.Util.fetch('<portlet:resourceURL/>', {
				method: 'GET',
				headers: {
					'Content-Type': 'text/html',
				}
			}).then(
				// response.text() e' una "promise"
				response => response.text()
			).then(function(response) {
				document
					.getElementById('<portlet:namespace />myDiv')
					.innerHTML = response;
			}).catch(function() {
				alert('error');
			});

		});
	}
</aui:script>

