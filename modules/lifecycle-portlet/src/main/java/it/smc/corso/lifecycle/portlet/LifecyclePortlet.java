package it.smc.corso.lifecycle.portlet;

import it.smc.corso.lifecycle.constants.LifecyclePortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.*;

import org.osgi.service.component.annotations.Component;

import java.io.IOException;

@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=corso",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true",
		"com.liferay.portlet.show-portlet-access-denied=false",
		"javax.portlet.display-name=Lifecycle",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + LifecyclePortletKeys.LIFECYCLE,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class LifecyclePortlet extends MVCPortlet {
	@Override
	public void destroy() {
		System.out.println("fase destroy");

		super.destroy();
	}

	@Override
	public void init() throws PortletException {
		System.out.println("fase init");

		super.init();
	}

	@Override
	public void processAction(
		ActionRequest actionRequest, ActionResponse actionResponse)
		throws IOException, PortletException {

		System.out.println("fase action");
	}

	@Override
	public void render(
		RenderRequest renderRequest, RenderResponse renderResponse)
		throws IOException, PortletException {

		System.out.println("fase render");

		super.render(renderRequest, renderResponse);
	}

	public void serveResource(
		ResourceRequest resourceRequest, ResourceResponse resourceResponse)
		throws IOException, PortletException {

		System.out.println("fase resource");

		resourceResponse.setContentType("text/html");
		resourceResponse.getWriter().write("Risorsa inviata!");
	}

}
