package it.smc.corso.lifecycle.constants;

/**
 * @author rge
 */
public class LifecyclePortletKeys {

	public static final String LIFECYCLE =
		"it_smc_corso_lifecycle_LifecyclePortlet";

}