#!/bin/sh

tent=60
while :; do

  echo "waiting $tent"

  #nc -w 1 postgres 5432
  curl -s postgres:5432
  result=$?

  if [ "$result" -eq 0 -o "$result" -eq 52 ] ; then
    break
  fi

  tent=$((tent - 1))

  if [ "$tent" -le 0 ]; then
    echo "timeout"
    break
  fi

  sleep 1

done
